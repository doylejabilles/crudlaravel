<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//git@gitlab.com:doylejabilles/crudlaravel.git


// Route::get('/', function () {
//     return view('welcome');
// });
// Route::resource('projects', 'ProjectsController');
Route::get('/projects','ProjectsController@index');
Route::get('/projects/{project}','ProjectsController@show');
Route::get('/projects/{project}/edit','ProjectsController@edit');
Route::patch('/projects/{project}','ProjectsController@update');
Route::delete('/projects/{project}','ProjectsController@destroy');
Route::post('/projects','ProjectsController@store');

Route::post('/tasks','TasksController@store');
Route::patch('/tasks/{id}','TasksController@update');
Auth::routes();

Route::get('/', function () {
    return redirect('/projects');
});

Route::get('/home', 'HomeController@index')->name('home');
