@extends('layout')

@section('content')

<div class="row">
    <div class="col-md-12">
        <a href="/projects" class="btn btn-default">Back</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                {{$project->title}}
            </div>
            <div class="card-body">
                {{ $project->description }}
            </div>
        </div>
        <a href="/projects/{{ $project->id }}/edit" class="btn btn-default">Edit</a>
    </div>
</div>

<!-- Tasks -->
@if ($project->tasks->count())
    <div class="row">
        <hr>
        <div class="col-md-12">
            <div class="card card-info">
                <div class="card-header">
                    <h3>Tasks</h3>
                </div>
                <div class="card-body">
                @foreach ($project->tasks as $task)
                    <form method="POST" action="/tasks/{{$task->id}}">
                        {{ method_field("PATCH") }}
                        {{ csrf_field() }}
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="completed" type="checkbox" onchange="this.form.submit()" {{ ($task->completed) ? 'checked' : '' }}>
                                {{ $task->description }}
                            </label>
                        </div>
                    </form>
                @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
<div class="row">
    <hr>
    @if( $errors->any() )
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <div class="card card-info">
            <div class="card-header">
                <h3>Add Task</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="/tasks">
                    @csrf
                    <input name="project_id" type="hidden" value="{{ $project->id }}">
                    <input name="description" class="form-control">
                    <button type="submit" class="btn btn-success">Add Task</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection