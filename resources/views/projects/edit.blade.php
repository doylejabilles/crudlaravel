@extends('layout')

@section('content')

<div class="row">
    <div class="col-md-12">
        <a href="/projects/" class="btn btn-default">Back</a>
    </div>
</div>

<div class="row">
    @if( $errors->any() )
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <form method="POST" action="/projects/{{$project->id}}">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" value="{{$project->title}}" name="title">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="5" name="description">{{ $project->description }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info">Update</button>
            </div>
        </form>

        <form method="POST" action="/projects/{{$project->id}}">
            {{ method_field("DELETE") }}
            {{ csrf_field() }}

            <div class="form-group">
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </form>
    </div>
</div>

@endsection