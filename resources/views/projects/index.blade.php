@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h2>Project List</h2>
        <ul style="list-style-type:none;">
            @foreach ($projects as $project)
            <li class="{{ $project->id }}"><a href="/projects/{{ $project->id }}">{{ $project->title }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
<hr>
<div class="row">
    @if( $errors->any() )
        <div class="col-md-12">
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="col-md-6">
        <h2>Add Project</h2>
        <form method="POST" action="/projects">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title">
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" rows="5" name="description"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Add Project</button>
            </div>
        </form>
    </div>
</div>
@endsection