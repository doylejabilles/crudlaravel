@extends('layout')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2>Project Lists</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @foreach ($projects as $project)
                    <li>{{ $project->title }}</li>
                @endforeach
            </div>
        </div>

    </div>

@endsection