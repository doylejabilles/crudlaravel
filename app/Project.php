<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title', 'owner_id', 'description'];

    public function tasks(){
        return $this->hasMany(Task::class);
    }
}
