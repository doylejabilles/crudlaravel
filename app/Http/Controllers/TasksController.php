<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task; //using task model

class TasksController extends Controller
{

    public function store() {

        request()->validate([
            'description' => 'required'
        ]);

        Task::create([
            'project_id' => request('project_id'),
            'description' => request('description')
        ]);
        return back();
    }
    
    public function update($id) {

        $task = Task::find($id);
        $task->update([
            'completed' => request()->has('completed')
        ]);
        
        return back();
    }
}
